<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/r1', function () {
    return view('r1');
});

Route::get('/storage/{path}', function ($path) {
    $file = new \Symfony\Component\HttpFoundation\File\File(storage_path($path));
    return response()->download($file, null, [], 'inline');
})->where('path', '(.*)');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/development', 'HomeController@development');

Route::put('/user/public', 'UserController@updatePublic');
Route::put('/user/private', 'UserController@updatePrivate');
Route::put('/user/profile', 'UserController@updateProfile');

Route::group(['prefix' => '/{email}'], function() {
    Route::get('/picture', 'EmailController@picture');
    Route::get('/data', 'EmailController@data');
});

Route::get('/test/login', function (){
    if(!request()->has('code')) {
        // Build the query parameter string to pass auth information to our request
        $query = http_build_query([
            'client_id' => 3,
            'redirect_uri' => '/test/login',
            'response_type' => 'code',
            'scope' => 'private'
        ]);

        // Redirect the user to the OAuth authorization page
        return redirect('/oauth/authorize?' . $query);
    }
    else {
        $request = Request::create('/oauth/token', 'POST', [
            'grant_type' => 'authorization_code',
            'client_id' => "3",
            'client_secret' => 'kB4FEx6lV5uHQ7fIOOTclsTrp2CH1RQdLPB8ym8N', // from admin panel above
            'redirect_uri' => '/test/login',
            'code' => request()->code // Get code from the callback
        ]);
        /** @var \Symfony\Component\HttpFoundation\Response $response */
        $response = app()->handle($request);

        $token = json_decode((string) $response->getContent(), true)['access_token'];

        $user = Request::create('/api/profile');
        $user->headers->add([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json'
        ]);

        $data = json_decode((string) app()->handle($user)->getContent(), true);

        return view('test.response', compact('token', 'data'));
    }
});

Route::post('/callback/test', function (Request $request)
{
    /*
     * For test async request
     */
    sleep(1);
    \Illuminate\Support\Facades\Log::info($request->all());
});