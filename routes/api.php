<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/private', function (Request $request) {
    return [
        'phone' => $request->user()->getProfile()->phone,
        'address' => $request->user()->getProfile()->address,
    ];

})->middleware('auth:api');

Route::get('/profile', function (Request $request) {
    return $request->user()->getProfile();

})->middleware('auth:api');
