<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DB;

class EmailController extends Controller
{
    /**
     * Get profile image
     *
     * @param Request $request
     * @param $mailHash
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function picture(Request $request, $mailHash)
    {
        /** @var User $user */
        $user = User::where(DB::raw('md5(email)'), $mailHash)->first();

        if(!$user) {
            return response()->download(new File(resource_path('assets/images/anon.png')), null, [], 'inline');
        }

        return response()->download($user->getProfile()->getPicture(), null, [], 'inline');
    }

    public function data(Request $request, $mailHash)
    {
        /** @var User $user */
        $user = User::where(DB::raw('md5(email)'), $mailHash)->first();

        if(!$user) throw new NotFoundHttpException();

        return response()->json($user->getProfile());
    }
}
