<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePublic(Request $request)
    {
        $this->validate($request, [
            'nickname' => 'min:3|max:20',
            'last_name' => 'required',
            'first_name' => 'required'
        ]);

        $data = $request->all();

        if($picture = $request->file('picture')) {
            $this->validate($request, [
                'picture' => 'mimes:jpeg,jpg,png,gif|required|max:5000'
            ]);

            $name = str_random();
            $extension = $picture->getClientOriginalExtension();

            $picture->move(storage_path('images'), "$name.$extension");

            $data['picture'] = "images/$name.$extension";
        }

        $profile = auth()->user()->getProfile();
        $profile->fill(collect($data)->only(['nickname', 'last_name', 'first_name', 'picture'])->toArray());
        $profile->user_id = auth()->user()->id;
        $profile->save();

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePrivate(Request $request)
    {
        $this->validate($request, [
            'phone' => 'min:3|max:20',
            'address' => 'min:3'
        ]);

        $profile = auth()->user()->getProfile();
        $profile->fill(collect($request->all())->only(['phone', 'address'])->toArray());
        $profile->user_id = auth()->user()->id;
        $profile->save();

        return response()->json([
            'saved' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'password' => 'required_with_all:new_password,new_password_repeat|old_password:' . auth()->user()->password,
            'new_password' => 'required_with:password|min:6',
            'new_password_repeat' => 'required_with:new_password|same:new_password'
        ]);

        auth()->user()->password = bcrypt($request->get('new_password'));
        auth()->user()->save();

        return response()->json([
            'saved' => true
        ]);
    }
}
