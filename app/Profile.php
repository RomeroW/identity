<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\File;

use Log;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'nickname', 'picture', 'phone', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'phone', 'address', 'user', 'picture', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getPictureAttribute($value)
    {
        return url('/' . md5($this->user->email) . '/picture');
    }

    public function getPicture()
    {
        return new File(storage_path($this->attributes['picture']));
    }

    public function save(array $options = [])
    {
        if(count($dirty = $this->getDirty())) {

            foreach ($this->user->tokens as $token) {
                $client = $token->client;

                if($client->update_callback) {
                    curl_post_async(trim(url($client->update_callback), '/')
                        . "?secret={$client->secret}&id=$this->user_id", $dirty);
                }
            }
        }

        return parent::save($options);
    }
}
