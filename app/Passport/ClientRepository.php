<?php

namespace App\Passport;

use Laravel\Passport\Client;

class ClientRepository
{
    /**
     * Get a client by the given ID.
     *
     * @param  int  $id
     * @return Client|null
     */
    public function find($id)
    {
        return Client::find($id);
    }

    /**
     * Get an active client by the given ID.
     *
     * @param  int  $id
     * @return Client|null
     */
    public function findActive($id)
    {
        $client = $this->find($id);

        return $client && ! $client->revoked ? $client : null;
    }

    /**
     * Get the client instances for the given user ID.
     *
     * @param  mixed  $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function forUser($userId)
    {
        return Client::where('user_id', $userId)
            ->orderBy('name', 'desc')->get();
    }

    /**
     * Get the active client instances for the given user ID.
     *
     * @param  mixed  $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function activeForUser($userId)
    {
        return $this->forUser($userId)->reject(function ($client) {
            return $client->revoked;
        })->values();
    }

    /**
     * Get the personal access token client for the application.
     *
     * @return Client
     */
    public function personalAccessClient()
    {
        if (Passport::$personalAccessClient) {
            return Client::find(Passport::$personalAccessClient);
        } else {
            return PersonalAccessClient::orderBy('id', 'desc')->first()->client;
        }
    }

    /**
     * Store a new client.
     *
     * @param  int $userId
     * @param  string $name
     * @param  string $redirect
     * @param string $updateCallback
     * @param  bool $personalAccess
     * @param  bool $password
     * @return Client
     */
    public function create($userId, $name, $redirect, $updateCallback = '', $personalAccess = false, $password = false)
    {
        $client = (new Client)->forceFill([
            'user_id' => $userId,
            'name' => $name,
            'secret' => str_random(40),
            'redirect' => $redirect,
            'update_callback' => $updateCallback,
            'personal_access_client' => $personalAccess,
            'password_client' => $password,
            'revoked' => false,
        ]);

        $client->save();

        return $client;
    }

    /**
     * Store a new personal access token client.
     *
     * @param  int $userId
     * @param  string $name
     * @param  string $redirect
     * @param string $updateCallback
     * @return Client
     */
    public function createPersonalAccessClient($userId, $name, $redirect, $updateCallback = '')
    {
        return $this->create($userId, $name, $redirect, $updateCallback, true);
    }

    /**
     * Store a new password grant client.
     *
     * @param  int $userId
     * @param  string $name
     * @param  string $redirect
     * @param string $updateCallback
     * @return Client
     */
    public function createPasswordGrantClient($userId, $name, $redirect, $updateCallback = '')
    {
        return $this->create($userId, $name, $redirect, $updateCallback, false, true);
    }

    /**
     * Update the given client.
     *
     * @param  Client $client
     * @param  string $name
     * @param  string $redirect
     * @param string $updateCallback
     * @return Client
     */
    public function update(Client $client, $name, $redirect, $updateCallback = '')
    {
        $client->forceFill([
            'name' => $name, 'redirect' => $redirect, 'update_callback' => $updateCallback
        ])->save();

        return $client;
    }

    /**
     * Regenerate the client secret.
     *
     * @param  Client  $client
     * @return Client
     */
    public function regenerateSecret(Client $client)
    {
        $client->forceFill([
            'secret' => str_random(40),
        ])->save();

        return $client;
    }

    /**
     * Determine if the given client is revoked.
     *
     * @param  int  $id
     * @return bool
     */
    public function revoked($id)
    {
        return Client::where('id', $id)
            ->where('revoked', true)->exists();
    }

    /**
     * Delete the given client.
     *
     * @param  Client  $client
     * @return void
     */
    public function delete(Client $client)
    {
        $client->tokens()->update(['revoked' => true]);

        $client->forceFill(['revoked' => true])->save();
    }
}
