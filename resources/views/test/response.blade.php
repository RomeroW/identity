<script type="text/javascript">
    @if($token && $data)
    window.opener.success('{{ $data['first_name'] }} {{ $data['last_name'] }}', '{{ $token }}');
    @endif
    window.close();
</script>