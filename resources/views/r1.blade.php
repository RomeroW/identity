<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ЛАМПА</title>

        <!-- Fonts -->
        <link href="/css/style.css" rel="stylesheet" type="text/css">

        <script>
            window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>

    <div class="navbar-light">
        <nav class="navbar navbar-fixed-top navbar-light">
            <div class="container">

                <!-- Brand -->
                <a class="navbar-brand" href="#">ЛАМПА<span class="red">.</span></a>

                <!-- Links -->
                <ul class="nav navbar-nav pull-xs-right">
                    <li class="nav-item">
                        <a class="nav-link lang-link" href="#">
                            <i class="flag-md flag-md-UA"></i>
                        </a>
                    </li>

                    @if (Route::has('login'))

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/login') }}">Увійти</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/register') }}">Реєстрація</a>
                        </li>
                    @endif

                </ul>
            </div>
        </nav>
        <div style="margin-top: 60px"></div>
    </div>

    <div class="jumbotron jumbotron-fluid jumbotron-main">
        <div class="container">
            <h1 class="display-3">Єбать.</h1>
            <p class="lead">Оренда квартир. Нарешті це зручно.</p>
        </div>
    </div>

    <div class="container container-bottom">
        <div class="row">
            <div class="col-sm-3 text-xs-center">
                <div class="display-3">1<span class="red">.</span></div>
                <div class="f-head">
                    Розміщуєте оголошення
                </div>
                <div class="detailed">Все дуже просто &mdash; лише два кліки. Завжди безкоштовно.</div>
            </div>
            <div class="col-sm-3 text-xs-center">
                <div class="display-3">2<span class="red">.</span></div>
                <div class="f-head">
                    Бачите підходящі об'єкти
                </div>
                <div class="detailed">Про всі нові пропозиції ми сповістимо Вас по email'у</div>
            </div>
            <div class="col-sm-3 text-xs-center">
                <div class="display-3">3<span class="red">.</span></div>
                <div class="f-head">
                    Відкриваєте свої контакти
                </div>

                <div class="detailed">Лише тим агентам, які справді Вас зацікавили</div>
            </div>
            <div class="col-sm-3 text-xs-center">
                <div class="display-3">4<span class="red">.</span></div>
                <div class="f-head">
                    Агент зв'язується з Вами
                </div>
                <div class="detailed">Ви також можете зв'язатися з агентом</div>
            </div>
        </div>
    </div>

    <div class="container container-bottom">
        <div class="row row-border-top">
            <div class="col-sm-6 offset-sm-3 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-xs-center top-20">
                Переймаєтеся, що Вас кинуть на гроші? Втомилися від неіснуючих квартир сумнівних ріелторів?
            </div>
            <div class="col-sm-4 offset-sm-4 text-xs-center top-20">
                <a href="#" class="btn btn-main-call-to-action">Шукаю квартиру</a>
            </div>
        </div>
    </div>

    <div class="gray-container container-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="h2 h-title">Як працює <strong>ЛАМПА</strong><span class="red">:</span></div>
                    <div class="light-item">
                        <p>
                            <strong>ЛАМПА</strong> - це сервіс, що об'єднує хороших агентів нерухомості
                            та хороших клієнтів.
                        </p>
                        <p>
                            На відмінну від всіх існуючих сайтів на ринку ми підійшли
                            до проблеми неактуальних та неякісних оголошень принципово інакше.
                        </p>
                        <p>
                            Коли агент у себе в кабінеті
                            розміщує пропозицію, всі люди, яким вона підходить, автоматично отримують повідомлення
                            про об'єкт. Далі, якщо людині подобається, вона приймає пропозицію, і після цього
                            агент бачить номер телефону людини, а людина - номер телефону агента. Потім
                            обговорення деталей та перегляд квартири.
                        </p>
                    </div>
                    <div class="light-item">
                        <div class="h2 h-title h-title-light">ТОП-5 агентів нерухомості на <strong>ЛАМПІ</strong><span class="red">:</span></div>
                    </div>
                    <div class="agents light-item ">
                        <img src="https://www.startupfesteurope.com/site/wp-content/uploads/2016/02/Tim_Cook-980x1024.jpg"
                             alt="" class="img-rounded">
                        <img src="https://pp.vk.me/c630628/v630628692/2a0a4/61j9ZOwQIlM.jpg"
                             alt="" class="img-rounded">
                        <img src="https://www.startupfesteurope.com/site/wp-content/uploads/2016/02/Tim_Cook-980x1024.jpg"
                             alt="" class="img-rounded">
                        <img src="https://www.startupfesteurope.com/site/wp-content/uploads/2016/02/Tim_Cook-980x1024.jpg"
                             alt="" class="img-rounded">
                        <img src="https://www.startupfesteurope.com/site/wp-content/uploads/2016/02/Tim_Cook-980x1024.jpg"
                             alt="" class="img-rounded">
                    </div>
                    <div class="light-item text-xs-center">
                        <a href="#" class="btn btn-outline-primary btn-large">Приєднатися до команди агентів</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="h2 h-title">Вже шукають з нами<span class="red">:</span></div>

                    <div class="search-item">
                        <div class="search-title">Однокімнатна, Поділ, до 12 тисяч.</div>
                        <div class="search-description">
                            Шукаю однокімнатну квариру, на Подолі. До 12000 грн,
                            біля Житнього ринку. Житиму одна, правда з котом.
                            Працюю, у вільний час читаю та дивлюся фільми.
                        </div>
                        <div class="search-proposals">
                            Отримано 4 пропозиції за останній день.
                        </div>
                    </div>

                    <div class="search-item">
                        <div class="search-title">Двокімнатна, Голосіїво, до 8 тисяч.</div>
                        <div class="search-description">
                            Разом з дівчиною шукаємо двокімнатну квартиру у Голосіївському
                            районі, до восьми тисяч. тихі, не буянимо.
                        </div>
                        <div class="search-proposals">
                            Отримано 7 пропозицій за останній день.
                        </div>
                    </div>
                    <div class="search-item">
                        <div class="search-title">Однокімнатна, Оболонь, до 7 тисяч.</div>
                        <div class="search-description">
                            Для себе шукаю однокімнатну квартиру, на Оболоні. Було б
                            чудово, якби квартира була видова та біля метро (5 хвилин максимум).
                        </div>
                        <div class="search-proposals">
                            Отримано 3 пропозиції за останній день.
                        </div>
                    </div>
                    <div class="search-item">
                        <div class="search-title">Трикімнатна, Солом'янський, до 6 тисяч.</div>
                        <div class="search-description">
                            Разом із сім'єю шукаємо трикімнатну на Солом'янці. Бюджет -
                            6000 гривень. З нами житиме пес та кіт.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-top" style="margin-bottom: 10em">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-xs-center top-20">
                    Знайдіть квартиру своєї мрії разом з нами. Безпечно, з перевіреними агентами.
                </div>
                <div class="col-sm-4 offset-sm-4 text-xs-center top-20">
                    <a href="#" class="btn btn-main-call-to-action">Розмістити оголошення</a>
                </div>
                <div class="col-sm-6 offset-sm-3 col-md-6 offset-md-3 col-lg-4 offset-lg-4 text-xs-center description small top-20">
                    Розміщення оголошення та користування сервісом безкоштовне
                </div>
            </div>
        </div>
    </div>

    <footer class="bd-footer text-muted">
        <div class="container">
            <ul class="bd-footer-links">
                <li><a href="https://github.com/twbs/bootstrap">GitHub</a></li>
                <li><a href="https://twitter.com/getbootstrap">Twitter</a></li>
                <li><a href="/examples/">Examples</a></li>
                <li><a href="/about/history/">About</a></li>
            </ul>
            <p>Designed and built with all the love in the world by <a href="https://twitter.com/mdo" target="_blank">@mdo</a> and <a href="https://twitter.com/fat" target="_blank">@fat</a>. Maintained by the <a href="https://github.com/orgs/twbs/people">core team</a> with the help of <a href="https://github.com/twbs/bootstrap/graphs/contributors">our contributors</a>.</p>
            <p>Currently v4.0.0-alpha.4. Code licensed <a rel="license" href="https://github.com/twbs/bootstrap/blob/master/LICENSE" target="_blank">MIT</a>, docs <a rel="license" href="https://creativecommons.org/licenses/by/3.0/" target="_blank">CC BY 3.0</a>.</p>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="/js/landing.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

    </body>
</html>
