<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="/css/landing.css" rel="stylesheet" type="text/css">

        <script>
            window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>

    <div class="navbar-light">
        <nav class="navbar navbar-fixed-top navbar-light">
            <div class="container">

                <!-- Brand -->
                <a class="navbar-brand" href="#">Identity</a>

                <!-- Links -->
                <ul class="nav navbar-nav pull-xs-right">
                    @if (Route::has('login'))

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/login') }}">Login</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/register') }}">Register</a>
                        </li>
                    @endif

                </ul>
            </div>
        </nav>
        <div style="margin-top: 60px"></div>
    </div>


    <div class="jumbotron jumbotron-fluid jumbotron-main">
        <div class="container">
            <h1 class="display-3">Identity.</h1>
            <p class="lead">One service. All data. Private data with OAuth. Identity made simple. Finally.</p>
        </div>
    </div>

    <div class="container container-bottom" id="try">
        <div class="row">
            <div class="col-xs-12">
                <h2>Want to try?</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h4>Comment box</h4>
                <comments>
                    <div style="margin-bottom: 50px">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"
                                       value="john.doe@gmail.com"
                                >
                                <small id="emailHelp" class="form-text text-muted">Use john.doe@gmail.com for example.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleTextarea">Comment</label>
                                <textarea class="form-control" id="exampleTextarea" rows="3">Just a test</textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <div>
                        <div class="comment">
                            <div class="head">
                                <img src="http://identity.dev:8080/e13743a7f1db7f4246badd6fd6ff54ff/picture" alt="" width="50px" height="50px"
                                     class="img-circle pull-xs-left">
                                <div class="title">John Doe</div>
                                <small>16:32, 21 Jun, 2012 by john_doe</small>
                            </div>
                            <p>Lorem ipsum dolor sit amet, wow! So, how can it be fixed, man?
                            </p>

                        </div>
                    </div>
                </comments>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-7">
                <h4>Private data</h4>
                <oauth-example>
                    <div class="row">
                        <div class="col-xs-6">
                            <p>
                                Email: <code>john.doe@gmail.com</code>
                            </p>
                            <p>
                                Password: <code>111111</code>
                            </p>
                            <button type="submit" class="btn btn-outline-primary">Auth me!</button>
                        </div>
                        <div class="col-xs-6">
                            <div class="description">Auth to see phone and address...</div>
                        </div>
                    </div>
                </oauth-example>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <h2>Documentation</h2>
            </div>
        </div>
    </div>

    <div class="container container-bottom" id="docs">
        <div class="row">
            <div class="col-xs-9">
                <div id="introduction-container" class="bottom-30">
                    <h3>Introduction</h3>
                    <p>
                        Identity - це простий сервіс єдиних даних. Ви можете використовувати даний сервіс для запезпечення
                        аутентифікації користувачів у себе на ресурсі. Також даний сервіс можна використовувати для доступу
                        до приватних даних людини, якщо це необхідно. Наприклад, коли Вам потрібно робити доставку за
                        адресою, оскільки через Identity персона може лишити такі дані
                        (<a href="#oauth-container">авторизація через OAuth</a>), і надати Вам доступ до них.
                    </p>
                    <p>
                        Якщо Ви авторизуєте людину через OAuth вам не потрібно перейматися через неактуальність даних -
                        вам потрібно лише налаштувати endpoint для випадку оновлення даних, на мати завжди актуальні
                        дані про людину.
                    </p>
                    <p>
                        Інтеграція не займе більше п'яти хвилин. Поїхали.
                    </p>
                </div>
                <div id="public-container" class="bottom-30">
                    <h3>Public data</h3>
                    <p>
                        Публічні дані отримуються через email людини, що зареєстрована у нашому сервісі.
                        До таких даних належать:
                    </p>
                    <ul>
                        <li>Фото</li>
                        <li>Ім'я</li>
                        <li>Прізвище</li>
                        <li>Nickname</li>
                    </ul>
                    <p>Для доступу до даних використовується хеш (MD5) email'у користувача.</p>

                    <h4>Отримання зображення</h4>
                    <table class="table api">
                        <tr>
                            <td>
                                <strong>Endpoint:</strong>
                            </td>
                            <td>
                                <code>/md5('email')/picture</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Method:</strong>
                            </td>
                            <td>
                                <code>GET</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Data:</strong>
                            </td>
                            <td>
                                <p>
                                    Аватар користувача, розміром 100х100
                                </p>
                            </td>
                        </tr>
                    </table>

                    <h4>Отримання всіх даних</h4>
                    <table class="table api">
                        <tr>
                            <td>
                                <strong>Endpoint:</strong>
                            </td>
                            <td>
                                <code>/md5('email')/data</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Method:</strong>
                            </td>
                            <td>
                                <code>GET</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Data:</strong>
                            </td>
                            <td>
                                <p>
                                    JSON, об'єкт, що містить всі відкриті дані поточного користувача.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Example:</strong>
                            </td>
                            <td>
<pre class="code">{
  "id": 1,
  "first_name": "John",
  "last_name": "Doe",
  "nickname": "doe_john",
}
</pre>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="private-container" class="bottom-30">
                    <h3>Private data</h3>
                    <p>
                        Приватні дані доступні лише після надання людиною доступу через протокол OAuth2.
                        До таких даних належать:
                    </p>
                    <ul>
                        <li>Номер телефону</li>
                        <li>Домашня адреса</li>
                    </ul>

                    <h4>Аутентифікація</h4>
                    <p>
                        Для аутентифікації потрібно передати необхідні заголовки:
                    </p>
                    <table class="table api">
                        <tr>
                            <td>
                                <strong>Accept:</strong>
                            </td>
                            <td>
                                <code>application/json</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Authorization:</strong>
                            </td>
                            <td>
                                <code>Bearer {access-token}</code>
                            </td>
                        </tr>
                    </table>

                    <h4>Отримання даних</h4>
                    <p>Отримання приватних даних користувача.</p>
                    <table class="table api">
                        <tr>
                            <td>
                                <strong>Endpoint:</strong>
                            </td>
                            <td>
                                <code>/api/private</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Method:</strong>
                            </td>
                            <td>
                                <code>GET</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Data:</strong>
                            </td>
                            <td>
                                <p>
                                    Об'єкт, що містить лише приватні дані авторизованого користувача
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Example:</strong>
                            </td>
                            <td>
<pre class="code">{
  "phone": "+380551234567",
  "address": "LA, 36010, BIG AVE E, 67",
}
</pre>
                            </td>
                        </tr>
                    </table>

                    <h4>Публічні дані</h4>
                    <p>Отримання відкритих даних користувача.</p>
                    <table class="table api">
                        <tr>
                            <td>
                                <strong>Endpoint:</strong>
                            </td>
                            <td>
                                <code>/api/user</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Method:</strong>
                            </td>
                            <td>
                                <code>GET</code>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Data:</strong>
                            </td>
                            <td>
                                <p>
                                    Об'єкт, що містить лише публічні дані авторизованого користувача
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Response Example:</strong>
                            </td>
                            <td>
<pre class="code">{
  "id": 1,
  "first_name": "John",
  "last_name": "Doe",
  "nickname": "doe_john",
}
</pre>
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="oauth-container" class="bottom-30">
                    <h3>OAuth2 authentication</h3>
                    <p>
                        Приватні дані користувачів доступні лише після аутентифікації через OAuth.
                    </p>
                    <p>
                        Для початку роботи необхідно зареєструватися та
                        <a href="{{ url('/development') }}" target="_blank">створити клієнт</a>, через який користувачі
                        будуть аутентифіковуватися.
                    </p>
                    <p>
                        Всі подальші дії відповідають <a href="https://oauth.net/2/" target="_blank">специфікації OAuth2</a>.
                    </p>
                    <h4>Оновлення даних</h4>
                    <p>
                        Також в клієнті можна вказати callback (повний endpoint), куди будуть надходити дані користувача
                        після їх оновлення. Дані будуть надсилатися за таким принципом: <br>
                        <code>POST: {callback}?code={your-client-secret}&id={updated-user-id}</code>
                    </p>
                    <p>А в тілі запиту масив оновлених даних:</p>
                    <pre class="code">{
  "phone": "+380661234567"
}</pre>
                    <p>
                        Як це працює можна глянути <a href="#try">тут</a>.
                    </p>
                </div>
            </div>
            <div class="col-xs-3">
                <nav class="navbar navbar-light">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="nav-link" href="#introduction-container" id="introduction-link">Introduction</a>
                        </li>
                        <li>
                            <a class="nav-link" href="#public-container" id="public-link">Public data</a>
                        </li>
                        <li>
                            <a class="nav-link" href="#private-container" id="private-link">Private data</a>
                        </li>
                        <li>
                            <a class="nav-link" href="#oauth-container" id="oauth-link">OAuth2 authentication</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="/js/landing.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

    </body>
</html>
