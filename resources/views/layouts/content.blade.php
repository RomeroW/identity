@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <div class="panel panel-default">
                    <div class="panel-heading">Menu</div>

                    <div class="panel-body">
                        <ul class="nav">
                            <li><a href="/home">Account</a></li>
                            <li><a href="/development">Development</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @yield('data')
            </div>
        </div>
    </div>
@endsection
