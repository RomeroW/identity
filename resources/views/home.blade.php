@extends('layouts.content')

@section('data')
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-unlock fa-fw" aria-hidden="true"></i>
            Public Available Data</div>

        <div class="panel-body">
            <p class="description small">This data is public available via email.</p>

            <v-form endpoint="/user/public" method="put">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="nickname">Nickname</label>
                        <input type="text"
                               value="{{ auth()->user()->getProfile()->nickname }}"
                               class="form-control" id="nickname" name="nickname" placeholder="Enter nickname">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="first_name">First Name</label>
                        <input type="text"
                               value="{{ auth()->user()->getProfile()->first_name }}"
                               class="form-control" id="first_name" name="first_name" placeholder="Enter first name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="last_name">Last Name</label>
                        <input type="text"
                               value="{{ auth()->user()->getProfile()->last_name }}"
                               class="form-control" id="last_name" name="last_name" placeholder="Enter last name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="picture">Avatar</label>
                    <v-image-selector name="picture" value="{{ auth()->user()->getProfile()->picture }}"></v-image-selector>
                </div>
            </v-form>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-lock fa-fw" aria-hidden="true"></i>
            Private Available Data</div>

        <div class="panel-body">
            <p class="description small">This data is available only after OAuth authorization and giving appropriate permissions.</p>

            <v-form endpoint="/user/private" method="put">
                <div class="form-group">
                    <label for="phone">Mobile phone</label>
                    <input type="text"
                           value="{{ auth()->user()->getProfile()->phone }}"
                           class="form-control" id="phone" name="phone" placeholder="Enter phone number">
                </div>

                <div class="form-group">
                    <label for="address">Address</label>
                    <textarea name="address"
                              class="form-control" id="address" rows="3" placeholder="300 BOYLSTON AVE E
SEATTLE WA 98102
USA">{{ auth()->user()->getProfile()->address }}</textarea>
                </div>
            </v-form>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-user fa-fw" aria-hidden="true"></i>
            Profile</div>

        <div class="panel-body">
            <v-form endpoint="/user/profile" method="put" v-on:form-submitted="clean('[data-clean]')">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="email">Email address</label>
                        <input type="email" readonly class="form-control" id="email" value="{{ auth()->user()->email }}" name="email" placeholder="Enter email">
                    </div>
                </div>

                <fieldset style="margin-top: 20px">

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="password">Current Password</label>
                            <input type="password" class="form-control" name="password" id="password" aria-describedby="firstNameHelp" data-clean>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="new_password">New password</label>
                            <input type="password" class="form-control" id="new_password" name="new_password" data-clean>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="new_password_repeat">Repeat new password</label>
                            <input type="password" class="form-control" id="new_password_repeat" name="new_password_repeat" data-clean>
                        </div>
                    </div>
                </fieldset>
            </v-form>
        </div>
    </div>
@endsection
